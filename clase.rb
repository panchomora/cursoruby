# Definimos la clase anfitrion
# utilizamos creando un objeto anfitrion
# a = Anfitrion.new("Francisco")	
# a.decir_hola
# a.decir_adios

class Anfitrion
	
	def initialize(nombre = "Hola Mundo")
		@nombre = nombre
	end

	def decir_hola
		puts "Hola #{@nombre}"
	end

	def decir_adios
		puts "Adiós #{@nombre}, vuelve pronto"
	end
end

a = Anfitrion.new("Francisco")
a.decir_hola
a.decir_adios